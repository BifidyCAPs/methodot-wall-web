module.exports = {
    assetsDir: 'static',

    devServer:{
        proxy: {
            '^/api':{
            target:'http://localhost:8000',
            changeOrigin: true
            }
        }
    },

    configureWebpack: {
        devtool: 'source-map'
    },

    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = "Methodot 用户留言簿";
                args[0].description = "Methodot, 您的一站式云原生在线开发协作平台";
                return args;
            })
    },

    transpileDependencies: [
      'vuetify'
    ]
}
